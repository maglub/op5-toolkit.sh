# Introduction

# AWS Metadata

To read out metadata from inside an AWS instance:

```
wget/curl http://169.254.169.254
wget -q -O - http://169.254.169.254/latest/meta-data/instance-id
```

Get more info:

````
EC2_INSTANCE_ID="`wget -q -O - http://169.254.169.254/latest/meta-data/instance-id || die \"wget instance-id has failed: $?\"`"
test -n "$EC2_INSTANCE_ID" || die 'cannot obtain instance-id'
EC2_AVAIL_ZONE="`wget -q -O - http://169.254.169.254/latest/meta-data/placement/availability-zone || die \"wget availability-zone has failed: $?\"`"
test -n "$EC2_AVAIL_ZONE" || die 'cannot obtain availability-zone'
EC2_REGION="`echo \"$EC2_AVAIL_ZONE\" | sed -e 's:\([0-9][0-9]*\)[a-z]*\$:\\1:'`"
````


## References
http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-metadata.html
http://stackoverflow.com/questions/625644/find-out-the-instance-id-from-within-an-ec2-machine

# OP5 Authorizations:

The minimum authorizations for this toolkit to work are:

* Nagios Auth   -> Configuration Information
* Api           -> Api Config
* Host          -> Host Add Delete
* Host          -> View all
* Host Template -> Host Template View All
* Configuration -> Export
* Misc          -> FILE
