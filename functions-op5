#=================================================================
# OP5 functions
# Author: Magnus Lubeck, KMG Group GmbH, magnus.luebeck@kmggroup.ch
# Date: 2015-02-20
#
# Description:
#
#   These functions are to be used to manage an OP5 monitoring service
#   through the api.
#
# Usage:
#
#   template files (json) to be found in ...
#   op5 user and passwords to be configured in ./app.conf
#
# Known issues:
#
#  - It is cumbersome to add a host service check with the same name as a hostgroup service check if the host is member of the hostgroup.
#    This is a known bug in the OP5 product API
#
#=================================================================


#========================================================
# Helper functions
#========================================================

function hostExist(){
  local hostName="$1"
  hostFound=$(curl --insecure -u "${op5User}:${op5Password}" -X GET https://${op5Server}/api/config/host/${hostName} --silent | grep "${hostName}" | wc -l)

  [ $hostFound -gt 0 ] && return 0
  return 1
}


#----------------------------------------------
# perf data functions
#----------------------------------------------
getPerfData(){

  local hostName="$1"
  local serviceName="$(echo $2 | sed -e 's/ /%20/g')"
  local ds="$3"
  
  op5Get "/beta/perfdata/fetch?host=${hostName}&service=${serviceName}&ds=${ds}" | sed -e 's/[0-9][0-9][0-9]$//'

  # host=magnus2&service=ping10&ds=pl
}

#----------------------------------------------
# EC2 host functions
#----------------------------------------------

function addEC2Host(){
  local hostName="$1"
  local ipAddresss="$2"
  local hostGroup="$3"
  local EC2Id="$4"
  local EC2Region="$5"

  [ -z "$ec2id" ] && ec2id="none"
  [ -z "$ec2region" ] && ec2region="none"

  local jsonOutput="$(getJsonEC2Host "$@")"

  op5Put "$jsonOutput" "/config/host/${hostName}" 
  local rc=$?
  
  return $rc


#  curl --insecure -k -H "content-type: application/json" -d "{\"host_name\":\"$hostName\",\"alias\":\"$hostName\",\"address\":\"${ipAddress}\",\"template\":\"default-host-template\",\"register\":\"1\",\"file_id\":\"etc\/hosts.cfg\",\"hostgroups\":[\"$hostGroup\"],\"contact_groups\":[],\"_EC2_ID\":\"${ec2Id}\",\"_EC2_REGION\":\"${ec2Region}\"}" "https://${op5Server}/api/config/host" -u "${op5User}:${op5Password}" --write-out %{http_code} --silent
  
}

function addEC2Elb(){
  local hostName="$1"
  local ipAddresss="$2"
  local hostGroup="$3"
  local ELBName="$4"
  local ELBRegion="$5"

  [ -z "$ec2id" ] && ec2id="none"
  [ -z "$ec2region" ] && ec2region="none"

  local jsonOutput="$(getJsonEC2Elb "$@")"

  op5Put "$jsonOutput" "/config/host/${hostName}" 
  local rc=$?
  
  return $rc


#  curl --insecure -k -H "content-type: application/json" -d "{\"host_name\":\"$hostName\",\"alias\":\"$hostName\",\"address\":\"${ipAddress}\",\"template\":\"default-host-template\",\"register\":\"1\",\"file_id\":\"etc\/hosts.cfg\",\"hostgroups\":[\"$hostGroup\"],\"contact_groups\":[],\"_EC2_ID\":\"${ec2Id}\",\"_EC2_REGION\":\"${ec2Region}\"}" "https://${op5Server}/api/config/host" -u "${op5User}:${op5Password}" --write-out %{http_code} --silent
  
}

#----------------------------------------------
# Host functions
#----------------------------------------------

function addHost(){
  local hostName="$1"
  local ipAddress="$2"
  local hostGroup="$3"

  local jsonOutput="$(getJsonHost "$@")"

  op5Put "$jsonOutput" "/config/host/${hostName}" 
  local rc=$?
  
  return $rc
  
}

function deleteHost(){
  local hostName="$1"
  op5Delete /config/host/$hostName
  local rc=$?
  
  return $rc
}

function listHosts(){

  res=$(op5Get "/filter/query?query=\[hosts\]all&format=json&columns=name&limit=10000000")
  #--- filter out name from json
  #echo -e ${res//,/\\n} | cut -d ":" -f2 | sed -e 's/["}]//g' -e 's/].*//'
  [ -n "${res}" ] && echo ${res} | sed -e 's/,/\'$'\n/g' | sed -e 's/^[^:]*://' -e 's/["}]//g' -e 's/].*//'
}

function listHostByName(){

  local hostName="$1"
  
  res=$(op5Get "/filter/query?query=\[hosts\]name=\"${hostName}\"&columns=name&format=json&limit=10000000")
  #--- filter out name from json
  [ "$res" = '[]200' ] && return 1
  echo -e ${res//,/\\n} | cut -d ":" -f2 | sed -e 's/["}]//g' -e 's/].*//'
  return 0
}


#----------------------------------------------
# Service check functions
#----------------------------------------------

function addServiceCheck(){
  local hostName="$1"
  local serviceCheckName="$( echo $2 | sed -e  's/ /%20/g')"
  local serviceCheckCommand="$3"
  local serviceCheckCommandArgs="$4"

  local jsonOutput="$(getJsonServiceCheck "$@")"

  op5Put "$jsonOutput" "/config/service/${hostName}%253B${serviceCheckName}?parent_type=host" 
  local rc=$?

  return $rc
}

function addServiceCheckByJson(){

  op5Post "$1" "/config/service"
  
  return $rc
}

function deleteServiceCheck(){
  local hostName="$1"
  local serviceCheckName="$(echo $2 | sed -e 's/ /%20/g')"
  
  op5Delete /config/service/${hostName}%253B${serviceCheckName}
  local rc=$?
  
  return $rc
}

function listServiceCheck(){
  local hostName="$1"
  local serviceCheckName="$2"
  
  [[ -n "$hostName" && -n "$serviceCheckName" ]] && { getServiceCheckByName "$hostName" "$serviceCheckName" ; return 0 ; }


  [[ -n "$hostName" ]] && { 
    op5Get "/config/service/?format=json&limit=10000000" | sed -e 's/},{/\'$'\n/g' | sed -e 's/",".*//' -e 's/^"name":"//' | awk -F";" -v HOSTNAME=$hostName '$1==HOSTNAME {print}'
#    op5Get "/config/services/${hostName}?format=json&limit=1000000"
	return 0
  }


  op5Get "/config/service/?format=json&limit=10000000" | sed -e 's/},{/\'$'\n/g' | sed -e 's/",".*//' -e 's/^"name":"//'
#  op5Get "/filter/query?query=\[services\]all&columns=host.name,description&limit=10000000" | sed -e 's/},{/\'$'\n/g'  
  
  return $rc
  
}

function getServiceCheckByName(){

  local hostName="$1"
  local serviceCheckName="$(echo $2 | sed -e 's/ /%20/g')" 
  
  [[ -n "$hostName" && -n "$serviceCheckName" ]] && { op5Get "/config/service/${hostName}%253B${serviceCheckName}?format=json&limit=1000000" ; return 0 ; }
  return 1
}

#----------------------------------------------
# Host group service check functions
#----------------------------------------------

function addHostGroupServiceCheck(){
  local hostGroupName="$1"
  local serviceCheckName="$2"
  local serviceCheckCommand="$3"
  local serviceCheckCommandArgs="$4"

  local jsonOutput="$(getJsonHostGroupServiceCheck "$@")"
  op5Put "$jsonOutput" "/config/service/${hostGroupName}%253B${serviceCheckName}" 
  local rc=$?
  return $rc
}

#----------------------------------------------
# Host group functions
#----------------------------------------------

function addHostGroup(){
  local hostGroupName="$1"

  local jsonOutput="$(getJsonHostGroup "$@")"

  op5Post "$jsonOutput" "/config/hostgroup" 
  local rc=$?

  return $rc
}

function deleteHostGroup(){
  local hostGroupName="$1"
  
  op5Delete "/config/hostgroup/${hostGroupName}"
  local rc=$?
  
  return $rc
}

function isHostGroup(){
  local hostGroupName="$1"
  
  local resCount=$(listHostGroupByName "$hostGroupName" | wc -l)
  [ $resCount -gt 0 ] && return 0
  return 1

}

function listHostgroups(){

  res=$(op5Get "/filter/query?query=\[hostgroups\]all&format=json&columns=name&limit=10000000")
  #--- filter out name from json
  [ "$res" = "[]200malu" ] && return 1
  echo -e ${res//,/\\n} | cut -d ":" -f2 | sed -e 's/["}]//g' -e 's/].*//'
  return 0
}

function listHostGroupByName(){

  local hostGroupName="$1"
  
  res=$(op5Get "/filter/query?query=\[hostgroups\]name=\"${hostGroupName}\"&columns=name&format=json&limit=10000000")
  #--- filter out name from json
  [ "$res" = '[]200' ] && return 1
  echo -e ${res//,/\\n} | cut -d ":" -f2 | sed -e 's/["}]//g' -e 's/].*//'
  return 0
}

function hostGroupExist(){
  listHostGroupByName "$1" > /dev/null 2>&1
  return $?
}
#========================================================
# JSON strings
#========================================================


function getJsonEC2Host(){

  local hostName="$1"
  local ipAddress="$2"
  local hostGroup="$(echo $3 | sed -e 's/,/","/g')"
  local EC2Id="$4"
  local EC2Region="$5"

#cat<<EOT
#{"host_name":"$hostName","alias":"$hostName","address":"${ipAddress}","template":"default-host-template","register":"1","file_id":"etc\/hosts.cfg","hostgroups":["$hostGroup"],"contact_groups":[],"_EC2_ID":"${EC2Id}","_EC2_REGION":"${EC2Region}"}
#EOT

  cat $this_dir/$jsonTemplateDir/EC2Host.json | sed -e 's/\$HOSTNAME\$/'"${hostName}"'/g' \
                                                     -e 's/\$HOSTALIAS\$/'"${hostName}"'/g' \
                                                     -e 's/\$HOSTADDRESS\$/'"${ipAddress}"'/g' \
                                                     -e 's/\$HOSTGROUPNAME\$/'"${hostGroup}"'/g' \
                                                     -e 's/\$EC2ID\$/'"${EC2Id}"'/g' \
                                                     -e 's/\$EC2REGION\$/'"${EC2Region}"'/g' \
													 -e 's/\$SERVICEDESC\$/'"${serviceCheckName}"'/g' \
													 -e 's/\$SERVICECHECKCOMMAND\$/'"${serviceCheckCommand}"'/g' \
													 -e 's/\$SERVICECHECKCOMMANDARG\$/'"${serviceCheckCommandArgs}"'/g'
}

function getJsonEC2Elb(){

  local hostName="$1"
  local ipAddress="$2"
  local hostGroup="$(echo $3 | sed -e 's/,/","/g')"
  local EC2Id="$4"
  local EC2Region="$5"

  cat $this_dir/$jsonTemplateDir/EC2Elb.json | sed -e 's/\$HOSTNAME\$/'"${hostName}"'/g' \
                                                     -e 's/\$HOSTALIAS\$/'"${hostName}"'/g' \
                                                     -e 's/\$HOSTADDRESS\$/'"${ipAddress}"'/g' \
                                                     -e 's/\$HOSTGROUPNAME\$/'"${hostGroup}"'/g' \
                                                     -e 's/\$EC2ID\$/'"${EC2Id}"'/g' \
                                                     -e 's/\$EC2REGION\$/'"${EC2Region}"'/g' \
													 -e 's/\$SERVICEDESC\$/'"${serviceCheckName}"'/g' \
													 -e 's/\$SERVICECHECKCOMMAND\$/'"${serviceCheckCommand}"'/g' \
													 -e 's/\$SERVICECHECKCOMMANDARG\$/'"${serviceCheckCommandArgs}"'/g'
}

function getJsonHostGroup(){
  local hostGroup="$1"

  cat $this_dir/$jsonTemplateDir/hostGroup.json | sed -e 's/\$HOSTNAME\$/'"${hostName}"'/g' \
                                                     -e 's/\$HOSTALIAS\$/'"${hostName}"'/g' \
                                                     -e 's/\$HOSTADDRESS\$/'"${ipAddress}"'/g' \
                                                     -e 's/\$HOSTGROUPNAME\$/'"${hostGroup}"'/g' \
                                                     -e 's/\$EC2ID\$/'"${EC2Id}"'/g' \
                                                     -e 's/\$EC2REGION\$/'"${EC2Region}"'/g' \
													 -e 's/\$SERVICEDESC\$/'"${serviceCheckName}"'/g' \
													 -e 's/\$SERVICECHECKCOMMAND\$/'"${serviceCheckCommand}"'/g' \
													 -e 's/\$SERVICECHECKCOMMANDARG\$/'"${serviceCheckCommandArgs}"'/g'
  
#cat<<EOT
#{"hostgroup_name":"${hostGroupName}","alias":"${hostGroupName}","notes":"","register":true,"file_id":"etc\/hostgroups.cfg","members":[],"hostgroup_members":[],"notes_url":"","action_url":""}
#EOT

}

function getJsonHost(){

  local hostName="$1"
  local ipAddress="$2"
  local hostGroup="$(echo $3 | sed -e 's/,/","/g')"

  cat $this_dir/$jsonTemplateDir/host.json | sed -e 's/\$HOSTNAME\$/'"${hostName}"'/g' \
                                                     -e 's/\$HOSTALIAS\$/'"${hostName}"'/g' \
                                                     -e 's/\$HOSTADDRESS\$/'"${ipAddress}"'/g' \
                                                     -e 's/\$HOSTGROUPNAME\$/'"${hostGroup}"'/g' \
                                                     -e 's/\$EC2ID\$/'"${EC2Id}"'/g' \
                                                     -e 's/\$EC2REGION\$/'"${EC2Region}"'/g' \
													 -e 's/\$SERVICEDESC\$/'"${serviceCheckName}"'/g' \
													 -e 's/\$SERVICECHECKCOMMAND\$/'"${serviceCheckCommand}"'/g' \
													 -e 's/\$SERVICECHECKCOMMANDARG\$/'"${serviceCheckCommandArgs}"'/g'

#cat<<EOT
#{"host_name":"$hostName","alias":"$hostName","address":"${ipAddress}","template":"default-host-template","register":"1","file_id":"etc\/hosts.cfg","hostgroups":["$hostGroup"],"contact_groups":[]}
#EOT

}
function getJsonServiceCheck(){

  local hostName="$1"
  local serviceCheckName="$2"
  local serviceCheckCommand="$3"
  local serviceCheckCommandArgs="$4"

  cat $this_dir/$jsonTemplateDir/serviceCheck.json | sed -e 's/\$HOSTNAME\$/'"${hostName}"'/g' \
                                                     -e 's/\$HOSTALIAS\$/'"${hostName}"'/g' \
                                                     -e 's/\$HOSTADDRESS\$/'"${ipAddress}"'/g' \
                                                     -e 's/\$HOSTGROUPNAME\$/'"${hostGroup}"'/g' \
                                                     -e 's/\$EC2ID\$/'"${EC2Id}"'/g' \
                                                     -e 's/\$EC2REGION\$/'"${EC2Region}"'/g' \
													 -e 's/\$SERVICEDESC\$/'"${serviceCheckName}"'/g' \
													 -e 's/\$SERVICECHECKCOMMAND\$/'"${serviceCheckCommand}"'/g' \
													 -e 's/\$SERVICECHECKCOMMANDARG\$/'"${serviceCheckCommandArgs}"'/g'

#  cat<<EOT
#{"host_name":"${hostName}","service_description":"${serviceCheckName}","check_command":"${serviceCheckCommand}","check_command_args":"${serviceCheckCommandArgs}","template":"default-service","register":true,"file_id":"etc\/services.cfg","is_volatile":false,"max_check_attempts":3,"check_interval":5,"retry_interval":1,"active_checks_enabled":true,"passive_checks_enabled":true,"check_period":"24x7","event_handler_enabled":true,"flap_detection_enabled":true,"process_perf_data":true,"retain_status_information":true,"retain_nonstatus_information":true,"notification_interval":0,"notification_period":"24x7","notification_options":["c","f","r","s","u","w"],"notifications_enabled":true,"hostgroup_name":"","display_name":"","servicegroups":[],"parallelize_check":"","obsess":"","check_freshness":"","freshness_threshold":"","event_handler":"","event_handler_args":"","low_flap_threshold":"","high_flap_threshold":"","flap_detection_options":[],"first_notification_delay":"","contacts":[],"contact_groups":[],"stalking_options":[],"notes":"","notes_url":"","action_url":"","icon_image":"","icon_image_alt":"","obsess_over_service":""}
#EOT


}

function getJsonHostGroupServiceCheck(){

  local hostGroup="$1"
  local serviceCheckName="$2"
  local serviceCheckCommand="$3"
  local serviceCheckCommandArgs="$4"

  cat $this_dir/$jsonTemplateDir/hostGroupServiceCheck.json | sed -e 's/\$HOSTNAME\$/'"${hostName}"'/g' \
                                                     -e 's/\$HOSTALIAS\$/'"${hostName}"'/g' \
                                                     -e 's/\$HOSTADDRESS\$/'"${ipAddress}"'/g' \
                                                     -e 's/\$HOSTGROUPNAME\$/'"${hostGroup}"'/g' \
                                                     -e 's/\$EC2ID\$/'"${EC2Id}"'/g' \
                                                     -e 's/\$EC2REGION\$/'"${EC2Region}"'/g' \
													 -e 's/\$SERVICEDESC\$/'"${serviceCheckName}"'/g' \
													 -e 's/\$SERVICECHECKCOMMAND\$/'"${serviceCheckCommand}"'/g' \
													 -e 's/\$SERVICECHECKCOMMANDARG\$/'"${serviceCheckCommandArgs}"'/g'


#  cat<<EOT
#{"hostgroup_name":"${hostGroupName}","service_description":"${serviceCheckName}","check_command":"${serviceCheckCommand}","check_command_args":"${serviceCheckCommandArgs}","template":"default-service","register":true,"file_id":"etc\/services.cfg","is_volatile":false,"max_check_attempts":3,"check_interval":5,"retry_interval":1,"active_checks_enabled":true,"passive_checks_enabled":true,"check_period":"24x7","event_handler_enabled":true,"flap_detection_enabled":true,"process_perf_data":true,"retain_status_information":true,"retain_nonstatus_information":true,"notification_interval":0,"notification_period":"24x7","notification_options":["c","f","r","s","u","w"],"notifications_enabled":true,"host_name":"","display_name":"","servicegroups":[],"parallelize_check":"","obsess":"","check_freshness":"","freshness_threshold":"","event_handler":"","event_handler_args":"","low_flap_threshold":"","high_flap_threshold":"","flap_detection_options":[],"first_notification_delay":"","contacts":[],"contact_groups":[],"stalking_options":[],"notes":"","notes_url":"","action_url":"","icon_image":"","icon_image_alt":"","obsess_over_service":""}
#EOT

}


#========================================================
# HTTP functions
#========================================================

#-------------------------------------------
# op5Get
#-------------------------------------------

function op5Get(){

  local URL="$(echo $1 | sed -e 's/ /%20/g')"

  [ -n "$debug" ] && set -x
  local res=$(curl --insecure "https://${op5Server}/api${URL}" -u "${op5User}:${op5Password}" --write-out %{http_code} --silent -X GET)
  local rc=$?

  if [[ -n "$debug" ||  -n "$(echo $res | grep error)" ]]
  then
    rc=1
	cat<<EOT
	
Error when running: 

  curl --insecure "https://${op5Server}/api${URL}" -u "${op5User}:${op5Password}" --write-out %{http_code} --silent -X GET

Error message:

$res

EOT
  else
    echo "$res"
  fi
  [ -n "$debug" ] && set +x  
  return $rc
}

#-------------------------------------------
# op5Put
#-------------------------------------------

function op5Put(){

  local JSON="$1"
  local URL="$(echo $2 | sed -e 's/ /%20/g')"

  [ -n "$debug" ] && set -x
  local res=$(curl --insecure -k -H "content-type: application/json" -d "${JSON}" "https://${op5Server}/api${URL}" -u "${op5User}:${op5Password}" --write-out %{http_code} --silent -X PUT)
  local rc=$?

  if [ -n "$(echo $res | grep error)" ]
  then
    rc=1
	cat<<EOT
	
Error when running: 

curl --insecure -k -H "content-type: application/json" -d '${JSON}' "https://${op5Server}/api${URL}" -u "${op5User}:${op5Password}" --write-out %{http_code} --silent -X PUT	

Error message:

$res

EOT
  fi
  [ -n "$debug" ] && set +x  
  return $rc
}

#-------------------------------------------
# op5Post
#-------------------------------------------

function op5Post(){

  local JSON="$1"
  local URL="$(echo $2 | sed -e 's/ /%20/g')"
  
  [ -n "$debug" ] && set -x
  [ -n "$JSON" ] && local res=$(curl --insecure -k -H "content-type: application/json" -d "${JSON}" "https://${op5Server}/api${URL}" -u "${op5User}:${op5Password}" --write-out %{http_code} --silent -X POST)
  [ -z "$JSON" ] && local res=$(curl --insecure -k "https://${op5Server}/api${URL}" -u "${op5User}:${op5Password}" --write-out %{http_code} --silent -X POST)

  local rc=$?

  if [ -n "$(echo $res | grep error)" ]
  then
    rc=1
	cat<<EOT
	
Error when running: 

curl --insecure -k -H "content-type: application/json" -d '${JSON}' "https://${op5Server}/api${URL}" -u "${op5User}:${op5Password}" --write-out %{http_code} --silent -X POST	

Error message:

$res

EOT
  fi
  [ -n "$debug" ] && set +x  
  return $rc
}

#-------------------------------------------
# op5Delete
#-------------------------------------------

function op5Delete(){

  local URL="$(echo $1 | sed -e 's/ /%20/g')"

  [ -n "$debug" ] && set -x
  local res=$(curl --insecure -k "https://${op5Server}/api${URL}" -u "${op5User}:${op5Password}" --write-out %{http_code} --silent -X DELETE)

  local rc=0

  if [ -n "$(echo $res | grep error)" ]
  then
    rc=1
	cat<<EOT
	
Error when running: 

curl --insecure -k -H "content-type: application/json" -d '${JSON}' "https://${op5Server}/api${URL}" -u "${op5User}:${op5Password}" --write-out %{http_code} --silent -X PUT	

Error message:

$res

EOT
  else
    echo "Delete on $URL ok!"
  fi
 
 
  [ -n "$debug" ] && set +x  
  return $rc
}


#========================================================
# consistency functions
#========================================================

function commit(){
  op5Post "" /config/change
  return $?
}

function rollback(){
  op5Delete /config/change
  return $?
#  curl --insecure -u "${op5User}:${op5Password}" -X DELETE https://${op5Server}/api/config/change
}

function listChanges(){
  curl --silent --insecure -u "$op5User:$op5Password" -X GET https://${op5Server}/api/config/change | sed -e 's/},/},\'$'\n/g'
  return 0
}

